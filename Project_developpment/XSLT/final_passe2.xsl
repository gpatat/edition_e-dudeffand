<?xml version="1.0" encoding="UTF-8"?>

<!-- On inclut la déclaration non prefixé de TEI en déclaration d'espace de nom par défaut pour ne ne pas avoir à la répéter dans chaque nouvelle
création de balise TEI. -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" 
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="3.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <!-- Suppression des espaces blancs en trop pour tous les éléments. -->
    <xsl:strip-space elements="*"/>
    <xsl:output indent="yes" method="xml" encoding="UTF-8"/>
    
    <!-- Copy source file : Ce bout de code permet de ne pas perdre d'informations lors de la transformation en copiant tous les nœuds du document source.-->
    <xsl:template match="@* | node()" mode="#all">
        <xsl:choose>
            <xsl:when test="matches(name(.), '^(part|instant|anchored|default|full|status)$')"/>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@* | node()" mode="#current"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Deuxième passe : création des éléments de plus bas niveau -->
    
    <!-- <dateline> du <opener> : on ajoute un @n pour différencier éléments du opener ou du closer-->
    <xsl:template match="teiCorpus/TEI/text/body/div/p[child::hi[@rend='underline color(0070C0)']]">
        <xsl:element name="dateline">
            <xsl:attribute name="n">opener</xsl:attribute>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <!-- <dateline> du <closer> -->
    <xsl:template match="teiCorpus/TEI/text/body/div/p[child::hi[@rend='underline color(FFC000)']]"> 
        <xsl:element name="dateline">
            <xsl:attribute name="n">closer</xsl:attribute>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <!-- <salute> du <opener> -->
    <xsl:template match="teiCorpus/TEI/text/body/div/p[child::hi[@rend='color(0070C0)']]">
        <xsl:element name="salute">
            <xsl:attribute name="n">opener</xsl:attribute>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <!-- <salute> du <closer>-->
    <xsl:template match="teiCorpus/TEI/text/body/div/p[child::hi[@rend='color(FFC000)' ]]">
        <xsl:element name="salute">
            <xsl:attribute name="n">closer</xsl:attribute>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <!-- <rs type="letter"> -->
    <xsl:template match="teiCorpus/TEI/text/body/div/p/hi[@rend='color(FF0000)']"> 
    <xsl:element name="objectName">
        <xsl:attribute name="type">letter</xsl:attribute>
        <xsl:attribute name="ref">#id_lettre</xsl:attribute>
        <xsl:value-of select="."/>
    </xsl:element>
    </xsl:template>
    
    <!-- <persName> -->
    <xsl:template match="teiCorpus/TEI/text/body/div/p/hi[@rend='bold color(00B050)' ]">
        <xsl:element name="persName">
            <xsl:attribute name="ref">#id_personne</xsl:attribute>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <!-- <rs type="work"> --> <!-- Modifie également l'expéditeur dans les métadonnées, mais n'a aucune importance puisque l'information a déjà été récupérée à la passe 1 et que le péritexte sera supprimé en passe 3.-->
    <xsl:template match="teiCorpus/TEI/text/body/div/p/hi[@rend='italic']"> 
        <xsl:element name="rs">
            <xsl:attribute name="type">work</xsl:attribute>
            <xsl:attribute name="ref">#id_oeuvre</xsl:attribute>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <!-- <rs type="person">  -->
    <xsl:template match="teiCorpus/TEI/text/body/div/p/hi[@rend='underline color(00B050)' ]">
        <xsl:element name="rs">
            <xsl:attribute name="type">person</xsl:attribute>
            <xsl:attribute name="ref">#id_personne</xsl:attribute>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <!-- <l> -->
       
    <xsl:template match="teiCorpus/TEI/text/body/div/p[child::hi[@rend='italic bold' ]]">
        <xsl:element name="l">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <!-- <quote> -->
    <xsl:template match="teiCorpus/TEI/text/body/div/p/hi[@rend='italic underline' ]">
        <xsl:element name="quote">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <!-- <date> -->
    <xsl:template match="teiCorpus/TEI/text/body/div/p/hi[@rend='underline']">
        <xsl:element name="date">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <!-- <signed> -->
    <xsl:template match="teiCorpus/TEI/text/body/div/p[child::hi[@rend='bold color(FFC000)' ]]">
        <xsl:element name="signed">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <!-- <postscript -->
    
    <xsl:template match="teiCorpus/TEI/text/body/div/p[child::hi[@rend='color(7030A0)' ]]">
        <xsl:element name="postscript">
            <xsl:element name="p">
            <xsl:value-of select="."/>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    

    

    
    
    

    
    

    
</xsl:stylesheet>